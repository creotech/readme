# README

Included below is a high level overview of the overall repository structure for Creo Software. It is divided into sections based on where the project is hosted in AWS (note: all projects in Creo currently are hosted by AWS as of this writing).

The three primary applications in Creo are creo-platform (the backend monolith), creo-frontend (the health coach browser tool), and creo-ionic (the patient-facing mobile app). Further details on them are below. Also included are details on other important repositories. Repositories not included in this README are ones that either I am not familiar with or ones that are no longer relevant and should be archived.

# EC2
creo-platform
  This is the backend monolith for creo. It is the primary server application and is hosted on prod-app1 and prod-app2 which can be accessed through (ssh prodapp-1, ssh prod-app2). The log file can be found on that instance at /usr/share/tomcat/logs/catalina.out. Tailing the log file will show any error messages or exceptions that were printed using the slf4j library. This server uses a mongodb database which we currently don't have access to outside of the application.

creo-frontend
  This is the browser tool used by health coaches to manage data on patients, to see patient info, bill for services, and various other tasks. This tool talks directly to the creo-platform to get its information and sends information to creo-platform where it is stored in the backend database.

creo-ionic
  This is the patient facing mobile app. 

creo-challenges
  This is the only microservice currently in use at creo....

creo-madre
  This is a less important project that was used as a temporary workaround to billing issues. Tonya will have more details. It is a jhipster CRUD tool that was used to manage claims in a really inefficient way until a better solution was being put in place which was almost completed. In the creo health coach tool there is now a "claims" tab on the far right that has features for managing claims that are missing info. Again, tonya will have more details.


# Lambdas
lambda-data-warehouse-export:
  This is the lambda that triggers the DataWareHouseService in the creo-platform to dump various data into a text file in an S3 bucket. Once the data is in the text file, the lambda in repository lambda-redshift-warehouse-import is triggered which then takes those text files and dumps their contents into the Redshift aws service where the data can be queries using some data analytics tool (we used to use one called Mode, but canceled the membership).

lambda-redshift-warehouse-import:
  This is the lambda described above that dumps data from s3 into redshift (postgresql).


# Tools
creo-queries
  This contains various javascript queries that were written to meet business needs at various times. They were run on the data imports ec2 instance (prod-app3) which is no longer running. They include various javascript queries and python script that converts the json output of those queries into csv. This might be helpful to gain insights on an approach to extracting data from the database that can be viewed in a spreadsheet.


